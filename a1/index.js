// Activity

// Create a new server that handles requests for user routes. 
// The following features must be included:

/*
    1. Get a list of all users.
    2. View a specific user's profile
    3. Update user information
    4. Delete a user
    5. Register a new user
*/

// Create a new Postman collection called "Node.js Activity 1" and add all the routes to that collection. 
// Make sure to test each one, and then export the collection to your a1 folder

let http  = require('http')

const port = 3000;

const server = http.createServer(function(request, response){
    if(request.method === 'GET' && request.url === '/route/users'){ // GET all users
        response.writeHead(200, {'Content-Type': 'text/plain'})        
        response.end(`User list`)        

    }else if(request.method === 'GET' && request.url === '/route/user_profile/123'){ // GET specific user
        response.writeHead(200, {'Content-Type': 'text/plain'})        
        response.end(`User 123 Profile`)       
              
    }else if(request.method === 'PUT' && request.url === '/route/user_profile_update/123'){ // PUT specific user
        response.writeHead(200, {'Content-Type': 'text/plain'})        
        response.end(`User 123 UPDATED`)   

    }else if(request.method === 'DELETE' && request.url === '/route/user_delete/123'){
        response.writeHead(200, {'Content-Type': 'text/plain'})        
        response.end(`User 123 DELETED`)  

    }else if(request.method === 'POST' && request.url === '/route/user_add'){
        response.writeHead(200, {'Content-Type': 'text/plain'})        
        response.end(`New User Added`)     

    }else{
        response.writeHead(200, {'Content-Type': 'text/plain'})        
        response.end('<h1>404! Page not found</h1>')        
    }   
})

server.listen(port)

console.log(`Listening to ${port}`)