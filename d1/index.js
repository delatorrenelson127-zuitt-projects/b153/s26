// Introduction to POSTMAN and REST
/*
When a user makes a request, the url endpoint is not the only
property included in that request. There is also n HTTP method that helps determine the exact operation for the server to accomplish
*/


// The four HTTP methods are as follows:

/*
    GET - Retrieve information about a resource
          By default, whenever a browser loads andy page, it makes a GET request

    POST - Used to create a new resource

    PUT -  Used to update information about an already-existing resource

    DELETE - Used to delete an already existing resource



    Scenario: You're using a website for infromation about music called www.coolmusicdb.com

    The first thing you want to see is a list of all the songs on the website.

    You go to www.coolmusicdb.com/api/songs - The browser is able to read both the URL endpoint of the address (/api/songs) that you provided, as well as the method of request that you are making.

    Since we are simply loading a page on a browser, the request that you send is automatically sent as GET request.

    The browser accpest your request, and creates the rueqes object with the folling information (and more):

    {
        url: "/api/songs",
        method: "GET"
    }

    and then shows you its entire list of songs.

    Next, you want to see specific information about one song.
    For example: "Happy Birthday to YOU"
*/

let http  = require('http')

const port = 4000;

const server = http.createServer(function(request, response){
    if(request.method === 'GET' && request.url === '/api/products'){
        response.writeHead(200, {'Content-Type': 'text/plain'})        
        response.end(`Products retrieved from database`)        

    }else if(request.method === 'GET' && request.url === '/api/products/123'){
        response.writeHead(200, {'Content-Type': 'text/plain'})        
        response.end(`Products 123 retrieved from database`)       
              
    }else if(request.method === 'PUT' && request.url === '/api/products/123'){
        response.writeHead(200, {'Content-Type': 'text/plain'})        
        response.end(`Products 123 updated from database`)   

    }else if(request.method === 'DELETE' && request.url === '/api/products/123'){
        response.writeHead(200, {'Content-Type': 'text/plain'})        
        response.end(`Products deleted from database`)  

    }else if(request.method === 'POST' && request.url === '/api/products'){
        response.writeHead(200, {'Content-Type': 'text/plain'})        
        response.end(`Products Added to database`)     

    }else{
        response.writeHead(200, {'Content-Type': 'text/plain'})        
        response.end('<h1>404! Page not found</h1>')        
    }   
})

server.listen(port)

console.log(`Listening to ${port}`)




